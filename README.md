Este é um fork do Django Builder Report para a tradução em PT-BR.



Informações sobre o Projeto Original:
# django-report-builder

A GUI for Django ORM. Build custom queries and display results.
Targets sys admins and capable end users who might not be able to program or gain direct interactive shell access.

[![pipeline status](https://gitlab.com/burke-software/django-report-builder/badges/master/pipeline.svg)](https://gitlab.com/burke-software/django-report-builder/commits/master)
[![coverage report](https://gitlab.com/burke-software/django-report-builder/badges/master/coverage.svg)](https://gitlab.com/burke-software/django-report-builder/commits/master)

# News

## 6.3

- Added Django 2.2 support. Django 1.11 and 2.1 are still supported.
- Unit tests finally run in Python 3.7 thanks to Celery supporting it
- Angular updated to version 7

## 6.2

- Added partial Python 3.7 support. We can't fully support Python 3.7 until Celery does.

## 6.1

- Added Django 2.1 support. 2.0 and 1.11 are still supported.


# What is Django Report Builder?

![](docs/screenshots/reportbuilderscreen.jpg)

## Features

* Add filters
* Add display fields
* Preview and create xlsx reports
* Very simple security, user must have change or "view" permission to view
  reports. Unprivileged users can still build reports and see database schema.
* Model properties (thanks yekibud)
* Export to Report global admin action
* Scheduled reports can generate and send to users on cron like schedule
* Optional asynchronous report generation

# Documentation

http://django-report-builder.readthedocs.org/

[Google group](https://groups.google.com/forum/#!forum/django-report-builder/).

[Contributing](http://django-report-builder.readthedocs.org/en/latest/contributors/)

## Development quick start

This package uses Django in Docker and Angular CLI for development purposes.

1. Start docker `docker-compose up`
2. Migrate and  create an admin user `docker-compose run --rm web ./manage.py migrate`
3. Start the Angular CLI server. Ensure Node is installed. `cd js`, `yarn`, `yarn start`
4. Django runs on port 8000 by default. Go to localhost:8000/admin and log in.
5. Angular runs on port 4200. Now that you are logged in, go to localhost:4200

More detailed instructions are at [here](https://django-report-builder.readthedocs.io/en/latest/contributors/)


# INFORMAÇÕES FORK

Para contribuir com o desenvolvimento:

Requisitos:

1. Instalar o Docker para com ele criar o ambiente de desenvolvimento [Docker Install](https://docs.docker.com/install/overview/)
2. Instalar o Docker-Compose [Compose Install](https://docs.docker.com/compose/install/)
3. Instalar o NodeJS [NodeJS Install](https://nodejs.org/en/download/package-manager/)
4. Instalar o Angular CLI como comando npm install -g @angular/cli


Executando o Projeto (contributors)

1. Vá até a pasta do repositório usando o comando cd no linux
2. Crie o ambiente de Desenvolvimento com o comando `docker-compose build`
2. Execute o Migrate da Base de Dados Django com  o comando `docker-compose run --rm web ./manage.py migrate`
3. Crie o super usuário admin com o comando `docker-compose run --rm web ./manage.py createsuperuser` - utilizar as credenciais que quiser
4. Rodar o servidor Django/Docker com `docker-compose up`
5. Logar na interface de admin do Django `localhost:8000/admin`
6. Rodar o install do Projeto Angular `cd js` `yarn install`
7. Iniciar o servidor yarn `yarn start --configuration=pt`
8. Acesse a interface Angular em `localhost:4200`

Instalando WHL - Versão mais Recente 6.3.2
 1. Vá até a pasta dist em django-report-builder/dist
 2. Instalar a versão mais recente da WHL do django_report_builder_pt com o comando `pip install django_report_builder_pt-VERSÃO-DESEJADA-py2.py3-none-any.whl`
 3.  Adicionar `report_builder` na lista de INSTALLED_APPS do seu projeto Django
